# Projecte accés a dades 2018/2019
## Dades del projecte
- *Autor*: Andreu Furió Benavent
- *Centre*: IES Lluis Simarro LaCabra
- *Asignatura*: Accés a Dades

## Finalitat
Crear una base de dades de videojocs retro

## Fonts
Dades obtingudes del fórum retropie.