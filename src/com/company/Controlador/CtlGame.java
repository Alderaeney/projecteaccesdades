package com.company.Controlador;

import com.company.Modelo.Game;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CtlGame extends CtlDOM {

    private static final String ET_GAME = "game";
    private static final String ET_PATH = "path";
    private static final String ET_NAME = "name";
    private static final String ET_DESC = "desc";
    private static final String ET_IMAGE = "image";
    private static final String ET_RATING = "rating";
    private static final String ET_RELEASEDATE = "releasedate";
    private static final String ET_DEVELOPER = "developer";
    private static final String ET_PUBLISHER = "publisher";
    private static final String ET_GENRE = "genre";
    private static final String ET_PLAYERS = "players";

    public static Game llegir(Element game){
        String ePath = getValorEtiqueta(ET_PATH, game);
        String eName = getValorEtiqueta(ET_NAME, game);
        String eDesc = getValorEtiqueta(ET_DESC, game);
        String eImage = getValorEtiqueta(ET_IMAGE, game);
        String eReleaseDate;
        try {
            eReleaseDate = getValorEtiqueta(ET_RELEASEDATE, game);
        }catch (Exception e){
            eReleaseDate = "19991231T000000";
        }
        String eDeveloper = getValorEtiqueta(ET_DEVELOPER, game);
        String ePublisher = getValorEtiqueta(ET_PUBLISHER, game);
        String eGenre = getValorEtiqueta(ET_GENRE, game);
        String ePlayers = getValorEtiqueta(ET_PLAYERS, game);
        try{
            Double eRating = 0.0;
            eRating = Double.valueOf(getValorEtiqueta(ET_RATING, game));
            return new Game(ePath, eName, eDesc, eImage, eRating, eReleaseDate, eDeveloper, ePublisher, eGenre, ePlayers);
        } catch (Exception e){
            return new Game(ePath, eName, eDesc, eImage, eReleaseDate, eDeveloper, ePublisher, eGenre, ePlayers);
        }
    }

    public static void escriure(Game game, Element arrel, Document doc){
        Element joc;
        joc = doc.createElement(ET_GAME);
        Element nouElement = doc.createElement(ET_NAME);
        nouElement.appendChild(doc.createTextNode(game.getName()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_DESC);
        nouElement.appendChild(doc.createTextNode(game.getDesc()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_IMAGE);
        nouElement.appendChild(doc.createTextNode(game.getImage()));
        joc.appendChild(nouElement);
        if (game.getRating() != null){
            nouElement = doc.createElement(ET_RATING);
            nouElement.appendChild(doc.createTextNode(String.valueOf(game.getRating())));
            joc.appendChild(nouElement);
        }
        nouElement = doc.createElement(ET_RELEASEDATE);
        nouElement.appendChild(doc.createTextNode(game.getReleaseDate()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_DEVELOPER);
        nouElement.appendChild(doc.createTextNode(game.getDeveloper()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_PUBLISHER);
        nouElement.appendChild(doc.createTextNode(game.getPublisher()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_GENRE);
        nouElement.appendChild(doc.createTextNode(game.getGenre()));
        joc.appendChild(nouElement);
        nouElement = doc.createElement(ET_PLAYERS);
        nouElement.appendChild(doc.createTextNode(String.valueOf(game.getPlayers())));
        joc.appendChild(nouElement);
        arrel.appendChild(joc);
    }


/* //DEPRECREATED
    private static Calendar procesaFecha(String releaseDate){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.valueOf(releaseDate.substring(0, 4)), Integer.valueOf(releaseDate.substring(4, 6)) - 1, Integer.valueOf(releaseDate.substring(6, 9)));
        return calendar;
    }
    */
}
