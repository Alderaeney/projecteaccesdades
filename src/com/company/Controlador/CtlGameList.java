package com.company.Controlador;


import com.company.Excepciones.GameListFolderExists;
import com.company.Modelo.Console;
import com.company.Modelo.Game;
import com.company.Modelo.GameList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class CtlGameList extends CtlDOM {

    private static final String ET_GAMELIST = "gameList";
    private File gameListFolder;
    private GameList gameList;
    private ArrayList<Document> documents;

    public CtlGameList() {
        gameList = new GameList();
    }

    public CtlGameList(GameList gameList) {
        this.gameList = gameList;
    }

    public CtlGameList(File gameListFolder, GameList gameList) {
        this.gameListFolder = gameListFolder;
        this.gameList = gameList;
    }

    public Document recuperar(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
        Document doc = null;
        doc = instanciarDocument(xmlFile);
        return doc;
    }

    public GameList leer() throws IOException, SAXException, ParserConfigurationException, NullPointerException {
        GameList gameList = new GameList();
        Console consola = new Console();
        int num = 0;
        for (File carpeta :
                gameListFolder.listFiles()) {
            gameList.add(new Console(carpeta.getName()));
            consola = gameList.get(num++);
            File xmlFile = new File(carpeta.getAbsolutePath() + "/gamelist.xml");
            Element console = recuperar(xmlFile).getDocumentElement();
            NodeList games = console.getChildNodes();
            for (int i = 0; i < games.getLength(); i++) {
                consola.add(CtlGame.llegir((Element) games.item(i)));
            }
        }
        return gameList;
    }

    public GameList leer(String ruta) throws IOException, SAXException, ParserConfigurationException, NullPointerException {
        GameList gameList = new GameList();
        Console consola = new Console();
        gameListFolder = new File(ruta);
        int num = 0;
        for (File carpeta :
                gameListFolder.listFiles()) {
            gameList.add(new Console(carpeta.getName()));
            consola = gameList.get(num++);
            File xmlFile = new File(carpeta.getAbsolutePath() + "/gamelist.xml");
            Document doc = recuperar(xmlFile);
            Element egamesList = doc.getDocumentElement();
            NodeList games = egamesList.getChildNodes();
            for (int i = 0; i < games.getLength(); i++) {
                if (games.item(i).getNodeType() == Node.ELEMENT_NODE)
                    consola.add(CtlGame.llegir((Element) games.item(i)));
            }
        }
        return gameList;
    }

    public void escriure(String ruta, GameList gamelist) throws Exception {
        File CGameList = new File(ruta);
        if (!CGameList.exists()){
            if (CGameList.isDirectory()){
                throw new GameListFolderExists(CGameList);
            } else {
                throw new Exception("No se ha podido crear la carpeta");
            }
        } else CGameList.mkdir();
        for (Console console :
                gamelist) {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element gamesList = doc.createElement(ET_GAMELIST);
            for (Game game :
                    console) {
                CtlGame.escriure(game, gamesList, doc);
            }
            File carpeta = new File(CGameList.getAbsolutePath() + "/" + console.getName());
            carpeta.mkdir();
            File XML = new File(carpeta.getAbsolutePath() + "/gamelist.xml");
            escriureDocumentAXml(doc, XML);
        }
    }



    public File getGameListFolder() {
        return gameListFolder;
    }

    public void setGameListFolder(File gameListFolder) {
        this.gameListFolder = gameListFolder;
    }

    public GameList getGameList() {
        return gameList;
    }

    public void setGameList(GameList gameList) {
        this.gameList = gameList;
    }
}
