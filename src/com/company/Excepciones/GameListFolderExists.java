package com.company.Excepciones;

import java.io.File;

public class GameListFolderExists extends Exception {

    private File carpeta;

    public GameListFolderExists(File carpeta) {
        this.carpeta = carpeta;
    }

    @Override
    public String toString() {
        return "La carpeta " + carpeta.getName() + " ya existe.";
    }
}
