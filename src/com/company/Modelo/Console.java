package com.company.Modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class Console extends ArrayList<Game> implements Serializable {

    private String name;

    public Console(String name) {
        this.name = name;
    }

    public Console() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void imprimir(){
        System.out.println("Console: " + name);
        for (Game game :
                this) {
            System.out.println(game);
        }
    }

    public Game retornarGame(String name){
        for (Game game :
                this)
            if (game.getName().equalsIgnoreCase(name)) return game;
        return null;
    }

    public void eliminarGame(Game game){
        if (this.size() == 1) this.clear();
        else this.remove(game);
    }
}
