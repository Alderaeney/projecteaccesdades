package com.company.Modelo;

import java.io.File;
import java.io.Serializable;
import java.util.Calendar;

public class Game implements Serializable {

    private String path;
    private String name;
    private String desc;
    private String image;
    private Double rating = null;
    private String releaseDate;
    private String developer;
    private String publisher;
    private String genre;
    private String players;

    public Game(String path, String name, String desc, String image, Double rating, String releaseDate, String developer, String publisher, String genre, String players) {
        this.path = path;
        this.name = name;
        this.desc = desc;
        this.image = image;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.publisher = publisher;
        this.genre = genre;
        this.players = players;
    }

    public Game(String path, String name, String desc, String image, String releaseDate, String developer, String publisher, String genre, String players) {
        this.path = path;
        this.name = name;
        this.desc = desc;
        this.image = image;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.publisher = publisher;
        this.genre = genre;
        this.players = players;
    }

    public Game() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Game{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", image='" + image + '\'' +
                ", rating=" + rating +
                ", releaseDate='" + releaseDate + '\'' +
                ", developer='" + developer + '\'' +
                ", publisher='" + publisher + '\'' +
                ", genre='" + genre + '\'' +
                ", players=" + players +
                '}';
    }
}
