package com.company.Modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class GameList extends ArrayList<Console> implements Serializable {

    public void imprimir(){
        for (Console console :
                this) {
            console.imprimir();
        }
    }

    public Console retornarConsola(String nombre){
        for (Console consola :
                this) {
            if (consola.getName().equalsIgnoreCase(nombre))
                return consola;
        }
        return null;
    }

    public void imprimirConsoles(){
        for (Console consola :
                this) {
            System.out.println("Consola: " + consola.getName());
        }
    }

    public void borrarConsola(Console consola){
        if (this.size() == 1){
            this.clear();
        } else this.remove(consola);
    }
}
