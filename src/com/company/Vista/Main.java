package com.company.Vista;

import com.company.Controlador.CtlGameList;
import com.company.Modelo.Console;
import com.company.Modelo.Game;
import com.company.Modelo.GameList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        String ruta, path, consName, name, desc, image, developer, publisher, genre, console, releaseDate;
        Double rating = 0.0;
        String players = "";
        int opcion = 100;
        boolean salir;
        char continuar, consOJoc;
        GameList gameList = new GameList();
        CtlGameList ctlGameList = new CtlGameList();


        do {
            salir = false;
            do {
                try {
                    menu();
                    opcion = scanner.nextInt();
                    salir = true;
                } catch (Exception e){
                    System.out.println("Debes escribir un numero.");
                    scanner.nextLine();
                }
            } while (!salir);
            switch (opcion){
                case 1:
                    System.out.print("Introduce la ruta hasta la carpeta dónde se guardan los XML: ");
                    scanner.nextLine();
                    ruta = scanner.nextLine();
                    try {
                        gameList = ctlGameList.leer(ruta);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SAXException e) {
                        e.printStackTrace();
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    gameList.imprimir();
                    break;

                case 3:
                    System.out.print("Quieres añadir una Consola o un Juego (c, j, N): ");
                    consOJoc = scanner.next().charAt(0);
                    if (consOJoc == 'c' || consOJoc == 'C'){
                        System.out.print("Introduce el nombre de la nueva consola: ");
                        scanner.nextLine();
                        consName = scanner.nextLine();
                        Console nuevaConsola = new Console(consName);
                        gameList.add(nuevaConsola);
                        System.out.print("¿Añadir juegos a la consola? (s, N): ");
                        continuar = scanner.next().charAt(0);
                        if (continuar == 's' || continuar == 'S'){
                            salir = false;
                        } else salir = true;
                        while (!salir){
                            System.out.print("Introduce la ruta dónde está guardado el juego: ");
                            scanner.nextLine();
                            path = scanner.nextLine();
                            System.out.print("Introduce el nombre del juego: ");
                            name = scanner.nextLine();
                            System.out.print("Introduce la descripción del juego: ");
                            desc = scanner.nextLine();
                            System.out.print("Introduce la ruta a la caratula del juego: ");
                            image = scanner.nextLine();
                            salir = false;
                            do {
                                try {
                                    System.out.print("Introduce la valoración del juego (double): ");
                                    rating = scanner.nextDouble();
                                    salir = true;
                                } catch (Exception e){
                                    System.out.println("Introduce un numero de coma flotante.");
                                    scanner.nextLine();
                                }
                            } while (!salir);
                            System.out.print("Introduce la fecha de salida (AAAAMMDD): ");
                            scanner.nextLine();
                            releaseDate = scanner.nextLine();
                            System.out.print("Introduce el desarrollador del juego: ");
                            developer = scanner.nextLine();
                            System.out.print("Introduce el publisher del juego: ");
                            publisher = scanner.nextLine();
                            System.out.print("Introduce el genero del juego: ");
                            genre = scanner.nextLine();
                            System.out.print("Introduce la cantidad de jugadores que soporta el juego: ");
                            players = scanner.nextLine();
                            nuevaConsola.add(new Game(path, name, desc, image, rating, releaseDate, developer, publisher, genre, players));

                            System.out.print("¿Añadir más juegos? (s, N): ");
                            continuar = scanner.next().charAt(0);
                            if (continuar == 's' || continuar == 'S'){
                                salir = false;
                            } else salir = true;
                        }


                    } else if (consOJoc == 'j' || consOJoc == 'J'){
                        gameList.imprimirConsoles();
                        scanner.nextLine();
                        consName = scanner.nextLine();
                        Console consola;
                        if ((consola = gameList.retornarConsola(consName)) != null){
                            do {
                                System.out.print("Introduce la ruta dónde está guardado el juego: ");
                                scanner.nextLine();
                                path = scanner.nextLine();
                                System.out.print("Introduce el nombre del juego: ");
                                name = scanner.nextLine();
                                System.out.print("Introduce la descripción del juego: ");
                                desc = scanner.nextLine();
                                System.out.print("Introduce la ruta a la caratula del juego: ");
                                image = scanner.nextLine();
                                salir = false;
                                do {
                                    try {
                                        System.out.print("Introduce la valoración del juego (double): ");
                                        rating = scanner.nextDouble();
                                        salir = true;
                                    } catch (Exception e){
                                        System.out.println("Introduce un numero de coma flotante.");
                                        scanner.nextLine();
                                    }
                                } while (!salir);
                                System.out.print("Introduce la fecha de salida (AAAAMMDD): ");
                                scanner.nextLine();
                                releaseDate = scanner.nextLine();
                                System.out.print("Introduce el desarrollador del juego: ");
                                developer = scanner.nextLine();
                                System.out.print("Introduce el publisher del juego: ");
                                publisher = scanner.nextLine();
                                System.out.print("Introduce el genero del juego: ");
                                genre = scanner.nextLine();
                                System.out.print("Introduce la cantidad de jugadores que soporta el juego: ");
                                players = scanner.nextLine();
                                consola.add(new Game(path, name, desc, image, rating, releaseDate, developer, publisher, genre, players));
                                System.out.print("¿Añadir más juegos? (s, N): ");
                                continuar = scanner.next().charAt(0);
                                if (continuar == 's' || continuar == 'S'){
                                    salir = false;
                                } else salir = true;
                            } while (!salir);

                        } else System.out.println("Consola no encontrada.");
                    } else System.out.println("Opción no reconocida.");
                    break;

                case 4:
                    System.out.print("Quieres añadir una Consola o un Juego (c, j, N): ");
                    consOJoc = scanner.next().charAt(0);
                    if (consOJoc == 'c' || consOJoc == 'C'){
                        gameList.imprimirConsoles();
                        System.out.print("Introduce el nombre de la consola: ");
                        scanner.nextLine();
                        name = scanner.nextLine();
                        Console consola;
                        if ((consola = gameList.retornarConsola(name)) != null){
                            gameList.borrarConsola(consola);
                            System.out.println("Consola borrada existosamente.");
                        } else System.out.println("Consola no encontrada.");
                    } else if (consOJoc == 'j' || consOJoc == 'J'){
                        gameList.imprimirConsoles();
                        System.out.print("Introduce el nombre de la consola de la que es el juego: ");
                        scanner.nextLine();
                        name = scanner.nextLine();
                        Console consola;
                        if ((consola = gameList.retornarConsola(name)) != null){
                            consola.imprimir();
                            System.out.print("Introduce el nombre del juego que quieres borrar: ");
                            name = scanner.nextLine();
                            Game juego;
                            if ((juego = consola.retornarGame(name)) != null){
                                consola.eliminarGame(juego);
                                System.out.println("Juego borrado exitosamente.");
                            } else System.out.println("Juego no encontrado.");
                        } else System.out.println("Consola no encontrada.");
                    } else System.out.println("Opción no reconocida.");
                    break;

                case 5:
                    try {
                        System.out.print("Introduce la ruta dónde guardar la nueva base de datos: ");
                        scanner.nextLine();
                        ruta = scanner.nextLine();
                        ctlGameList.escriure(ruta, gameList);
                        System.out.println("Base de datos escrita exitosamente.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case 0:
                    System.out.println("Saliendo.");
                    break;

                default:
                    System.out.println("Opción no reconocida.");
                    break;
            }

        } while (opcion != 0);
    }

    private static final void menu(){
        System.out.print("1.- Recuperar archivos XML\n" +
                "2.- Mostrar gamelist\n" +
                "3.- Anadir consola y/o juegos\n" +
                "4.- Eliminar consola o juego\n" +
                "5.- Escribir GameList en documento\n" +
                "0.- Salir\n" +
                "-> ");
    }
}
